import express from 'express';
import { PrismaClient } from '@prisma/client';
import {initializeUsers} from "./entities/users";
import {initializeFriendship} from "./entities/friendship";
import {initializeConversation} from "./entities/conversation";
import {initializeConversationMessage} from "./entities/conversationMessage";
import {initializeNotifications} from "./entities/notifications";
import {initializePost} from "./entities/post";
import {initializePostComment} from "./entities/postComment";
import {initializePostLike} from "./entities/postLike";

const cors = require('cors');
const app = express();
const prisma = new PrismaClient();

const port: number = 3333;
app.use(express.json(), cors());


initializeUsers(app, prisma);
initializeFriendship(app, prisma);
initializeConversation(app, prisma);
initializeConversationMessage(app, prisma);
initializeNotifications(app, prisma);
initializePost(app, prisma);
initializePostComment(app, prisma);
initializePostLike(app, prisma);

app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});
//  v1-pwa-api
