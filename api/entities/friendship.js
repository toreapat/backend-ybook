"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.initializeFriendship = void 0;
function initializeFriendship(app, prisma) {
    var _this = this;
    // Create a new friendship
    app.post('/friend/', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var _a, fromId, toId, status_1, newFriendship, error_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 2, , 3]);
                    _a = req.body, fromId = _a.fromId, toId = _a.toId, status_1 = _a.status;
                    return [4 /*yield*/, prisma.friendship.create({
                            data: {
                                from: { connect: { id: fromId } },
                                to: { connect: { id: toId } },
                                status: status_1
                            }
                        })];
                case 1:
                    newFriendship = _b.sent();
                    res.json(newFriendship);
                    return [3 /*break*/, 3];
                case 2:
                    error_1 = _b.sent();
                    res.status(500).json(error_1);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    // Get all friendships
    app.get('/friends/', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var friendships, error_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, prisma.friendship.findMany()];
                case 1:
                    friendships = _a.sent();
                    res.json(friendships);
                    return [3 /*break*/, 3];
                case 2:
                    error_2 = _a.sent();
                    res.status(500).json(error_2);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    // Get a specific friendship by id
    app.get('/friend/:id', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var friendship, error_3;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, prisma.friendship.findUnique({
                            where: {
                                id: Number(req.params.id)
                            }
                        })];
                case 1:
                    friendship = _a.sent();
                    if (!friendship) {
                        return [2 /*return*/, res.status(404).json({ error: 'Friendship not found' })];
                    }
                    res.json(friendship);
                    return [3 /*break*/, 3];
                case 2:
                    error_3 = _a.sent();
                    res.status(500).json(error_3);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    // Update a specific friendship by id
    app.put('/friend/:id', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var status_2, friendship, error_4;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    status_2 = req.body.status;
                    return [4 /*yield*/, prisma.friendship.update({
                            where: {
                                id: Number(req.params.id)
                            },
                            data: {
                                status: status_2
                            }
                        })];
                case 1:
                    friendship = _a.sent();
                    if (!friendship) {
                        return [2 /*return*/, res.status(404).json({ error: 'Friendship not found' })];
                    }
                    res.json(friendship);
                    return [3 /*break*/, 3];
                case 2:
                    error_4 = _a.sent();
                    res.status(500).json(error_4);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    // Delete a specific friendship by id
    app["delete"]('/friend/:id', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var friendship, error_5;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, prisma.friendship["delete"]({
                            where: {
                                id: Number(req.params.id)
                            }
                        })];
                case 1:
                    friendship = _a.sent();
                    if (!friendship) {
                        return [2 /*return*/, res.status(404).json({ error: 'Friendship not found' })];
                    }
                    res.json({ message: 'Friendship deleted' });
                    return [3 /*break*/, 3];
                case 2:
                    error_5 = _a.sent();
                    res.status(500).json(error_5);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
}
exports.initializeFriendship = initializeFriendship;
