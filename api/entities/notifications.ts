import express, {Request, Response} from 'express';
import {PrismaClient} from '@prisma/client';

export function initializeNotifications(app: express.Application, prisma: PrismaClient) {

    interface Notifications {
        read: boolean;
        friendshipId: number;
        conversationMessageId: number;
    }

    app.post('/notification/', async (req: Request, res: Response) => {
        try {
            const {read, friendshipId, conversationMessageId}: Notifications = req.body;
            const notifications = await prisma.notification.create({
                data: {
                    read: read,
                    Friendship: {connect: {id: friendshipId}},
                    Message: {connect: {id: conversationMessageId}}
                },
            });
            res.json(notifications);
        } catch (error) {
            res.status(500).json(error)
        }
    });

// Get all notifications
    app.get('/notifications/', async (req: Request, res: Response) => {
        try {
            const notifications = await prisma.notification.findMany()
            res.json(notifications)
        } catch (error) {
            res.status(500).json(error)
        }
    })

// Get a specific notification by id
    app.get('/notification/:id', async (req: Request, res: Response) => {
        try {
            const notification = await prisma.notification.findUnique({
                where: {
                    id: Number(req.params.id)
                }
            })
            if (!notification) {
                return res.status(404).json({error: 'Notification message not found'})
            }
            res.json(notification)
        } catch (error) {
            res.status(500).json(error)
        }
    })

// Update a specific notification by id
    app.put('/notification/:id', async (req: Request, res: Response) => {
        try {
            const {read, friendshipId, conversationMessageId}: Notifications = req.body;
            const notification = await prisma.notification.update({
                where: {id: Number(req.params.id)},
                data: {
                    read: read,
                    friendshipId: friendshipId,
                    conversationMessageId: conversationMessageId

                },
            });
            if (!notification) {
                return res.status(404).send('Notification message not found');
            }
            res.json(notification);
        } catch (error) {
            res.status(500).json(error)
        }
    });

// Delete a specific notification by id
    app.delete('/notification/:id', async (req: Request, res: Response) => {
        try {
            const notificationExist = await prisma.notification.findUnique({
                where: {id: Number(req.params.id)},
            });
            if (!notificationExist) {
                return res.status(404).send('Notification message not found');
            }
            await prisma.notification.delete({
                where: {id: Number(req.params.id)},
            });
            res.status(200).send(`Notification message with id ${req.params.id} has been deleted`);
        } catch (error) {
            res.status(500).json(error);
        }
    });
}
