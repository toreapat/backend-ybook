import express, { Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';
export function initializeConversationMessage(app: express.Application, prisma: PrismaClient) {

    interface ConversationMessageData {
        conversationId: number;
        userId: number;
    }

    app.post('/conversation_message/', async (req: Request, res: Response) => {
        try {
            const {conversationId, userId}: ConversationMessageData = req.body;
            const newConversationMessage = await prisma.conversationMessage.create({
                data: {
                    Conversation: {connect: {id: conversationId}},
                    From: {connect: {id: userId}},
                },
            });
            res.json(newConversationMessage);
        } catch (error) {
            res.status(500).json(error)
        }
    });

// Get all conversations
    app.get('/conversation_messages/', async (req: Request, res: Response) => {
        try {
            const conversations = await prisma.conversationMessage.findMany()
            res.json(conversations)
        } catch (error) {
            res.status(500).json(error)
        }
    })

// Get a specific conversation by id
    app.get('/conversation_message/:id', async (req: Request, res: Response) => {
        try {
            const conversation = await prisma.conversationMessage.findUnique({
                where: {
                    id: Number(req.params.id)
                }
            })
            if (!conversation) {
                return res.status(404).json({error: 'Conversation message not found'})
            }
            res.json(conversation)
        } catch (error) {
            res.status(500).json(error)
        }
    })

// Update a specific conversation by id
    app.put('/conversation_message/:id', async (req: Request, res: Response) => {
        try {
            const {conversationId, userId}: ConversationMessageData = req.body;
            const conversation = await prisma.conversationMessage.update({
                where: {id: Number(req.params.id)},
                data: {
                    Conversation: {connect: {id: conversationId}},
                    From: {connect: {id: userId}},
                },
            });
            if (!conversation) {
                return res.status(404).send('Conversation message not found');
            }
            res.json(conversation);
        } catch (error) {
            res.status(500).json(error)
        }
    });

// Delete a specific conversation by id
    app.delete('/conversation_message/:id', async (req: Request, res: Response) => {
        try {
            const conversationExist = await prisma.conversationMessage.findUnique({
                where: {id: Number(req.params.id)},
            });
            if (!conversationExist) {
                return res.status(404).send('Conversation message not found');
            }
            await prisma.conversationMessage.delete({
                where: {id: Number(req.params.id)},
            });
            res.status(200).send(`Conversation message with id ${req.params.id} has been deleted`);
        } catch (error) {
            res.status(500).json(error);
        }
    });
}
