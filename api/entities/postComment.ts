import express, {Request, Response} from 'express';
import {PrismaClient} from '@prisma/client';

export function initializePostComment(app: express.Application, prisma: PrismaClient) {
    interface PostComment {
        text: string;
        userId: number;
        postId: number;
    }

    app.post('/post_comment/', async (req: Request, res: Response) => {
        try {
            const {text, userId, postId}: PostComment = req.body;
            const postComment = await prisma.postComment.create({
                data: {
                    text,
                    User: {connect: {id: userId}},
                    Post: {connect: {id: postId}}
                },
            });
            res.json(postComment);
        } catch (error) {
            res.status(500).json(error)
        }
    });

// Get all postComments
    app.get('/post_comments/', async (req: Request, res: Response) => {
        try {
            const postComment = await prisma.postComment.findMany()
            res.json(postComment)
        } catch (error) {
            res.status(500).json(error)
        }
    })

// Get a specific postComment by id
    app.get('/post_comment/:id', async (req: Request, res: Response) => {
        try {
            const postComment = await prisma.postComment.findUnique({
                where: {
                    id: Number(req.params.id)
                }
            })
            if (!postComment) {
                return res.status(404).json({error: 'PostComment not found'})
            }
            res.json(postComment)
        } catch (error) {
            res.status(500).json(error)
        }
    })

// Update a specific post by id
    app.put('/post_comment/:id', async (req: Request, res: Response) => {
        try {
            const {text, userId, postId}: PostComment = req.body;
            const postComment = await prisma.postComment.update({
                where: {
                    id: Number(req.params.id)
                },
                data: {
                    text: text,
                    User: {connect: {id: userId}},
                    Post: {connect: {id: postId}}
                },
            });
            if (!postComment) {
                return res.status(404).send('PostComment not found');
            }
            res.json(postComment);
        } catch (error) {
            res.status(500).json(error)
        }
    });

// Delete a specific post by id
    app.delete('/post_comment/:id', async (req: Request, res: Response) => {
        try {
            const postCommentExist = await prisma.postComment.findUnique({
                where: {id: Number(req.params.id)},
            });
            if (!postCommentExist) {
                return res.status(404).send('PostComment not found');
            }
            await prisma.postComment.delete({
                where: {id: Number(req.params.id)},
            });
            res.status(200).send(`PostComment with id ${req.params.id} has been deleted`);
        } catch (error) {
            res.status(500).json(error);
        }
    });
}
