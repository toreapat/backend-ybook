import express, {Request, Response} from 'express';
import {PrismaClient} from '@prisma/client';

export function initializePostLike(app: express.Application, prisma: PrismaClient) {
    interface PostLike {
        userId: number;
        postId: number;
    }

    app.post('/post_like/', async (req: Request, res: Response) => {
        try {
            const {userId, postId}: PostLike = req.body;
            const postLike = await prisma.postLike.create({
                data: {
                    User: {connect: {id: userId}},
                    Post: {connect: {id: postId}}
                },
            });
            res.json(postLike);
        } catch (error) {
            res.status(500).json(error)
        }
    });

// Get all PostLikes
    app.get('/post_likes/', async (req: Request, res: Response) => {
        try {
            const postLike = await prisma.postLike.findMany()
            res.json(postLike)
        } catch (error) {
            res.status(500).json(error)
        }
    })

// Get a specific PostLike by id
    app.get('/post_like/:id', async (req: Request, res: Response) => {
        try {
            const postLike = await prisma.postLike.findUnique({
                where: {
                    id: Number(req.params.id)
                }
            })
            if (!postLike) {
                return res.status(404).json({error: 'PostLike not found'})
            }
            res.json(postLike)
        } catch (error) {
            res.status(500).json(error)
        }
    })

// Update a specific PostLike by id
    app.put('/post_like/:id', async (req: Request, res: Response) => {
        try {
            const {userId, postId}: PostLike = req.body;
            const postLike = await prisma.postLike.update({
                where: {
                    id: Number(req.params.id)
                },
                data: {
                    User: {connect: {id: userId}},
                    Post: {connect: {id: postId}}
                },
            });
            if (!postLike) {
                return res.status(404).send('PostLike not found');
            }
            res.json(postLike);
        } catch (error) {
            res.status(500).json(error)
        }
    });

// Delete a specific post by id
    app.delete('/post_like/:id', async (req: Request, res: Response) => {
        try {
            const postLikeExist = await prisma.postLike.findUnique({
                where: {id: Number(req.params.id)},
            });
            if (!postLikeExist) {
                return res.status(404).send('PostLike not found');
            }
            await prisma.postLike.delete({
                where: {id: Number(req.params.id)},
            });
            res.status(200).send(`PostLike with id ${req.params.id} has been deleted`);
        } catch (error) {
            res.status(500).json(error);
        }
    });
}
