import express, {Request, Response} from 'express';
import {PrismaClient} from '@prisma/client';
import * as AWS from 'aws-sdk';

export function initializeUsers(app: express.Application, prisma: PrismaClient) {

const cognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider({region: process.env.REGION});

//Confirm forgot password
    app.post('/user/confirm_forgot_password/',async(req:Request, res: Response) => {
        const { confirmationCode, newPassword, email } = req.body;

        const params = {
            ClientId: `${process.env.CLIENT_ID}`,
            ConfirmationCode: confirmationCode,
            Password: newPassword,
            Username: email
        };

        await cognitoIdentityServiceProvider.confirmForgotPassword(params, function(err, data) {
            if (err) {
                res.status(400).json({ message: 'Wrong confirmation code', error: err });
            } else {
                res.json({ message: `Password changed successfully ${JSON.stringify(data)}` });
            }
        });
    })

//Forgot password
    app.post('/user/forgot_password/', async(req: Request, res: Response) => {
        const { email } = req.body;
        const params = {
            ClientId: `${process.env.CLIENT_ID}`,
            Username: email
        };
        await cognitoIdentityServiceProvider.forgotPassword(params, (err, data) => {
            if (err) {
                res.status(400).json({ message: 'The password reinitialization failed', error: err });
            } else {
                res.json({ message: `A code validation is sent to you for your forgotten password ${JSON.stringify(data)}` });
            }
        });
    });

//Logout
    app.post('/user/logout/', async (req: Request, res: Response) => {
        const accessToken = req.body;
        try {
            const params = {
                AccessToken: accessToken
            };

            await cognitoIdentityServiceProvider.globalSignOut(params)
            res.status(200).json({message: "Successfully log out"})
        } catch (error) {
            res.status(400).json({error: error});
        }
    });

//Update password
    app.put('/user/change_password/', async (req: Request, res: Response) => {
        const {accessToken, previousPassword, proposedPassword} = req.body;
        try {
            const params = {
                AccessToken: accessToken,
                PreviousPassword: previousPassword,
                ProposedPassword: proposedPassword
            };

            await cognitoIdentityServiceProvider.changePassword(params).promise();
            res.status(200).json({message: 'Password successfully changed'});
        } catch (error) {
            res.status(400).json({error: error});
        }
    });

//Login user
    app.post('/user/login/', async (req: Request, res: Response) => {
        const {email, password} = req.body;
        try {
            const params = {
                AuthFlow: 'USER_PASSWORD_AUTH',
                ClientId: `${process.env.CLIENT_ID}`,
                AuthParameters: {
                    USERNAME: email,
                    PASSWORD: password,
                },
            };
            const cognitoUser = await cognitoIdentityServiceProvider.initiateAuth(params).promise();

            res.json({message: 'User logged in', cognitoUser});
        } catch (err) {
            res.status(400).json({message: err});
        }
    });


//Register user
    app.post('/user/register/', async (req: Request, res: Response) => {
        const {email, password, name} = req.body;

        try {
            const params = {
                ClientId: `${process.env.CLIENT_ID}`,
                Password: password,
                Username: email,
                UserAttributes: [{Name: 'name', Value: name}],
            };
            await cognitoIdentityServiceProvider.signUp(params).promise();

            res.json({message: 'User registered'});
        } catch (err) {
            res.status(400).json({message: err});
        }
    });

//Confirm user
    app.post('/user/confirm/', async (req: Request, res: Response) => {
        const {email, confirmationCode} = req.body;
        const params = {
            ClientId: `${process.env.CLIENT_ID}`,
            ConfirmationCode: confirmationCode,
            Username: email,
        };

        try {
            await cognitoIdentityServiceProvider.confirmSignUp(params).promise();
            res.json({message: 'User confirmed'});
        } catch (err) {
            res.status(400).json({message: err});
        }
    });


// Create a new user
    app.post('/user/', async (req: Request, res: Response) => {
        try {
            const newUser = await prisma.user.create({
                data: req.body
            });
            res.status(201).json(newUser);
        } catch (error) {
            console.log('error:', error)
            res.status(400).json(error);
        }
    });

// Get all users
    app.get('/users/', async (req: Request, res: Response) => {
        try {
            const users = await prisma.user.findMany();
            res.status(200).json(users);
        } catch (error) {
            res.status(500).json(error);
        }
    });

// Get a specific user by ID
    app.get('/user/:id', async (req: Request, res: Response) => {
        try {
            const user = await prisma.user.findUnique({
                where: {
                    id: Number(req.params.id)
                }
            });
            if (!user) {
                return res.status(404).json({message: 'User not found'});
            }
            res.status(200).json(user);
        } catch (error) {
            res.status(500).json(error);
        }
    });

// Update a specific user by ID
    app.put('/user/:id', async (req: Request, res: Response) => {
        try {
            const user = await prisma.user.update({
                where: {
                    id: Number(req.params.id)
                },
                data: req.body
            });
            if (!user) {
                return res.status(404).json({message: 'User not found'});
            }
            res.status(200).json(user);
        } catch (error) {
            res.status(500).json(error);
        }
    });

// Delete a specific user by ID
    app.delete('/user/:id', async (req: Request, res: Response) => {
        try {
            const user = await prisma.user.delete({
                where: {
                    id: Number(req.params.id)
                }
            });
            if (!user) {
                return res.status(404).json({message: 'User not found'});
            }
            res.status(204).send();
        } catch (error) {
            res.status(500).json(error);
        }
    });
}
