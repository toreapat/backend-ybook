import express, {Request, Response} from 'express'
import {PrismaClient, FriendshipStatus} from '@prisma/client'

export function initializeFriendship(app: express.Application, prisma: PrismaClient) {

    interface FriendshipRequest {
        fromId: number;
        toId: number;
        status: FriendshipStatus;
    }

// Create a new friendship
    app.post('/friend/', async (req: Request, res: Response) => {
        try {
            const {fromId, toId, status}: FriendshipRequest = req.body;
            const newFriendship = await prisma.friendship.create({
                data: {
                    from: {connect: {id: fromId}},
                    to: {connect: {id: toId}},
                    status: status
                }
            })
            res.json(newFriendship)
        } catch (error) {
            res.status(500).json(error)
        }
    })

// Get all friendships
    app.get('/friends/', async (req: Request, res: Response) => {
        try {
            const friendships = await prisma.friendship.findMany()
            res.json(friendships)
        } catch (error) {
            res.status(500).json(error)
        }
    })

// Get a specific friendship by id
    app.get('/friend/:id', async (req: Request, res: Response) => {
        try {
            const friendship = await prisma.friendship.findUnique({
                where: {
                    id: Number(req.params.id)
                }
            })
            if (!friendship) {
                return res.status(404).json({error: 'Friendship not found'})
            }
            res.json(friendship)
        } catch (error) {
            res.status(500).json(error)
        }
    })

// Update a specific friendship by id
    app.put('/friend/:id', async (req: Request, res: Response) => {
        try {
            const {status}: FriendshipRequest = req.body;
            const friendship = await prisma.friendship.update({
                where: {
                    id: Number(req.params.id)
                },
                data: {
                    status: status
                }
            })
            if (!friendship) {
                return res.status(404).json({error: 'Friendship not found'})
            }
            res.json(friendship)
        } catch (error) {
            res.status(500).json(error)
        }
    })

// Delete a specific friendship by id
    app.delete('/friend/:id', async (req: Request, res: Response) => {
        try {
            const friendship = await prisma.friendship.delete({
                where: {
                    id: Number(req.params.id)
                }
            })
            if (!friendship) {
                return res.status(404).json({error: 'Friendship not found'})
            }
            res.json({message: 'Friendship deleted'})
        } catch (error) {
            res.status(500).json(error)
        }
    })

}
