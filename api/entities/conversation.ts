import express, {Request, Response} from 'express';
import {PrismaClient} from '@prisma/client';
export function initializeConversation(app: express.Application, prisma: PrismaClient) {
    interface ConversationData {
        fromId: number;
        toId: number;
    }


// Create a new conversation
    app.post('/conversation/', async (req: Request, res: Response) => {
        try {
            const {fromId, toId}: ConversationData = req.body;
            const newConv = await prisma.conversation.create({
                data: {
                    From: {connect: {id: fromId}},
                    To: {connect: {id: toId}},
                }
            })
            res.json(newConv)
        } catch (error) {
            res.status(500).json(error)
        }
    });

// Get all conversations
    app.get('/conversations/', async (req: Request, res: Response) => {
        try {
            const conversations = await prisma.conversation.findMany()
            res.json(conversations)
        } catch (error) {
            res.status(500).json(error)
        }
    })

// Get a specific conversation by id
    app.get('/conversation/:id', async (req: Request, res: Response) => {
        try {
            const conversation = await prisma.conversation.findUnique({
                where: {
                    id: Number(req.params.id)
                }
            })
            if (!conversation) {
                return res.status(404).json({error: 'Conversation not found'})
            }
            res.json(conversation)
        } catch (error) {
            res.status(500).json(error)
        }
    })


// Update a specific conversation by id
    app.put('/conversation/:id', async (req: Request, res: Response) => {
        try {
            const conversation = await prisma.conversation.update({
                where: {id: Number(req.params.id)},
                data: {
                    From: {connect: {id: req.body.fromId}},
                    To: {connect: {id: req.body.toId}},
                },
            });
            if (!conversation) {
                return res.status(404).send('Conversation not found');
            }
            res.json(conversation);
        } catch (error) {
            res.status(500).json(error)
        }
    });

// Delete a specific conversation by id
    app.delete('/conversation/:id', async (req: Request, res: Response) => {
        try {
            const conversationExist = await prisma.conversation.findUnique({
                where: {id: Number(req.params.id)},
            });
            if (!conversationExist) {
                return res.status(404).send('Conversation not found');
            }
            await prisma.conversation.delete({
                where: {id: Number(req.params.id)},
            });
            res.status(200).send(`Conversation with id ${req.params.id} has been deleted`);
        } catch (error) {
            res.status(500).json(error);
        }
    });
}
