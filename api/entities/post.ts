import express, {Request, Response} from 'express';
import {PrismaClient} from '@prisma/client';
export function initializePost(app: express.Application, prisma: PrismaClient) {

    interface Post {
        htmlContent: string;
        userId: number;
    }

    app.post('/post/', async (req: Request, res: Response) => {
        try {
            const {htmlContent, userId,}: Post = req.body;
            const post = await prisma.post.create({
                data: {
                    htmlContent,
                    User: {connect: {id: userId}}
                },
            });
            res.json(post);
        } catch (error) {
            res.status(500).json(error)
        }
    });

// Get all post
    app.get('/posts/', async (req: Request, res: Response) => {
        try {
            const post = await prisma.post.findMany()
            res.json(post)
        } catch (error) {
            res.status(500).json(error)
        }
    })

// Get a specific post by id
    app.get('/post/:id', async (req: Request, res: Response) => {
        try {
            const post = await prisma.post.findUnique({
                where: {
                    id: Number(req.params.id)
                }
            })
            if (!post) {
                return res.status(404).json({error: 'Post message not found'})
            }
            res.json(post)
        } catch (error) {
            res.status(500).json(error)
        }
    })

// Update a specific post by id
    app.put('/post/:id', async (req: Request, res: Response) => {
        try {
            const {htmlContent, userId}: Post = req.body;
            const post = await prisma.post.update({
                where: {id: Number(req.params.id)},
                data: {
                    htmlContent,
                    User: {connect: {id: userId}}
                },
            });
            if (!post) {
                return res.status(404).send('Post message not found');
            }
            res.json(post);
        } catch (error) {
            res.status(500).json(error)
        }
    });

// Delete a specific post by id
    app.delete('/post/:id', async (req: Request, res: Response) => {
        try {
            const postExist = await prisma.post.findUnique({
                where: {id: Number(req.params.id)},
            });
            if (!postExist) {
                return res.status(404).send('Post not found');
            }
            await prisma.post.delete({
                where: {id: Number(req.params.id)},
            });
            res.status(200).send(`Post with id ${req.params.id} has been deleted`);
        } catch (error) {
            res.status(500).json(error);
        }
    });
}
