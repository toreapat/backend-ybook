"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.initializeUsers = void 0;
var AWS = require("aws-sdk");
function initializeUsers(app, prisma) {
    var _this = this;
    var cognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider({ region: process.env.REGION });
    //Confirm forgot password
    app.post('/user/confirm_forgot_password/', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var _a, confirmationCode, newPassword, email, params;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _a = req.body, confirmationCode = _a.confirmationCode, newPassword = _a.newPassword, email = _a.email;
                    params = {
                        ClientId: "".concat(process.env.CLIENT_ID),
                        ConfirmationCode: confirmationCode,
                        Password: newPassword,
                        Username: email
                    };
                    return [4 /*yield*/, cognitoIdentityServiceProvider.confirmForgotPassword(params, function (err, data) {
                            if (err) {
                                res.status(400).json({ message: 'Wrong confirmation code', error: err });
                            }
                            else {
                                res.json({ message: "Password changed successfully ".concat(JSON.stringify(data)) });
                            }
                        })];
                case 1:
                    _b.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    //Forgot password
    app.post('/user/forgot_password/', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var email, params;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    email = req.body.email;
                    params = {
                        ClientId: "".concat(process.env.CLIENT_ID),
                        Username: email
                    };
                    return [4 /*yield*/, cognitoIdentityServiceProvider.forgotPassword(params, function (err, data) {
                            if (err) {
                                res.status(400).json({ message: 'The password reinitialization failed', error: err });
                            }
                            else {
                                res.json({ message: "A code validation is sent to you for your forgotten password ".concat(JSON.stringify(data)) });
                            }
                        })];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    //Logout
    app.post('/user/logout/', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var accessToken, params, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    accessToken = req.body;
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    params = {
                        AccessToken: accessToken
                    };
                    return [4 /*yield*/, cognitoIdentityServiceProvider.globalSignOut(params)];
                case 2:
                    _a.sent();
                    res.status(200).json({ message: "Successfully log out" });
                    return [3 /*break*/, 4];
                case 3:
                    error_1 = _a.sent();
                    res.status(400).json({ error: error_1 });
                    return [3 /*break*/, 4];
                case 4: return [2 /*return*/];
            }
        });
    }); });
    //Update password
    app.put('/user/change_password/', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var _a, accessToken, previousPassword, proposedPassword, params, error_2;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _a = req.body, accessToken = _a.accessToken, previousPassword = _a.previousPassword, proposedPassword = _a.proposedPassword;
                    _b.label = 1;
                case 1:
                    _b.trys.push([1, 3, , 4]);
                    params = {
                        AccessToken: accessToken,
                        PreviousPassword: previousPassword,
                        ProposedPassword: proposedPassword
                    };
                    return [4 /*yield*/, cognitoIdentityServiceProvider.changePassword(params).promise()];
                case 2:
                    _b.sent();
                    res.status(200).json({ message: 'Password successfully changed' });
                    return [3 /*break*/, 4];
                case 3:
                    error_2 = _b.sent();
                    res.status(400).json({ error: error_2 });
                    return [3 /*break*/, 4];
                case 4: return [2 /*return*/];
            }
        });
    }); });
    //Login user
    app.post('/user/login/', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var _a, email, password, params, cognitoUser, err_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _a = req.body, email = _a.email, password = _a.password;
                    _b.label = 1;
                case 1:
                    _b.trys.push([1, 3, , 4]);
                    params = {
                        AuthFlow: 'USER_PASSWORD_AUTH',
                        ClientId: "".concat(process.env.CLIENT_ID),
                        AuthParameters: {
                            USERNAME: email,
                            PASSWORD: password
                        }
                    };
                    return [4 /*yield*/, cognitoIdentityServiceProvider.initiateAuth(params).promise()];
                case 2:
                    cognitoUser = _b.sent();
                    res.json({ message: 'User logged in', cognitoUser: cognitoUser });
                    return [3 /*break*/, 4];
                case 3:
                    err_1 = _b.sent();
                    res.status(400).json({ message: err_1 });
                    return [3 /*break*/, 4];
                case 4: return [2 /*return*/];
            }
        });
    }); });
    //Register user
    app.post('/user/register/', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var _a, email, password, name, params, err_2;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _a = req.body, email = _a.email, password = _a.password, name = _a.name;
                    _b.label = 1;
                case 1:
                    _b.trys.push([1, 3, , 4]);
                    params = {
                        ClientId: "".concat(process.env.CLIENT_ID),
                        Password: password,
                        Username: email,
                        UserAttributes: [{ Name: 'name', Value: name }]
                    };
                    return [4 /*yield*/, cognitoIdentityServiceProvider.signUp(params).promise()];
                case 2:
                    _b.sent();
                    res.json({ message: 'User registered' });
                    return [3 /*break*/, 4];
                case 3:
                    err_2 = _b.sent();
                    res.status(400).json({ message: err_2 });
                    return [3 /*break*/, 4];
                case 4: return [2 /*return*/];
            }
        });
    }); });
    //Confirm user
    app.post('/user/confirm/', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var _a, email, confirmationCode, params, err_3;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _a = req.body, email = _a.email, confirmationCode = _a.confirmationCode;
                    params = {
                        ClientId: "".concat(process.env.CLIENT_ID),
                        ConfirmationCode: confirmationCode,
                        Username: email
                    };
                    _b.label = 1;
                case 1:
                    _b.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, cognitoIdentityServiceProvider.confirmSignUp(params).promise()];
                case 2:
                    _b.sent();
                    res.json({ message: 'User confirmed' });
                    return [3 /*break*/, 4];
                case 3:
                    err_3 = _b.sent();
                    res.status(400).json({ message: err_3 });
                    return [3 /*break*/, 4];
                case 4: return [2 /*return*/];
            }
        });
    }); });
    // Create a new user
    app.post('/user/', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var newUser, error_3;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, prisma.user.create({
                            data: req.body
                        })];
                case 1:
                    newUser = _a.sent();
                    res.status(201).json(newUser);
                    return [3 /*break*/, 3];
                case 2:
                    error_3 = _a.sent();
                    console.log('error:', error_3);
                    res.status(400).json(error_3);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    // Get all users
    app.get('/users/', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var users, error_4;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, prisma.user.findMany()];
                case 1:
                    users = _a.sent();
                    res.status(200).json(users);
                    return [3 /*break*/, 3];
                case 2:
                    error_4 = _a.sent();
                    res.status(500).json(error_4);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    // Get a specific user by ID
    app.get('/user/:id', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var user, error_5;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, prisma.user.findUnique({
                            where: {
                                id: Number(req.params.id)
                            }
                        })];
                case 1:
                    user = _a.sent();
                    if (!user) {
                        return [2 /*return*/, res.status(404).json({ message: 'User not found' })];
                    }
                    res.status(200).json(user);
                    return [3 /*break*/, 3];
                case 2:
                    error_5 = _a.sent();
                    res.status(500).json(error_5);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    // Update a specific user by ID
    app.put('/user/:id', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var user, error_6;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, prisma.user.update({
                            where: {
                                id: Number(req.params.id)
                            },
                            data: req.body
                        })];
                case 1:
                    user = _a.sent();
                    if (!user) {
                        return [2 /*return*/, res.status(404).json({ message: 'User not found' })];
                    }
                    res.status(200).json(user);
                    return [3 /*break*/, 3];
                case 2:
                    error_6 = _a.sent();
                    res.status(500).json(error_6);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    // Delete a specific user by ID
    app["delete"]('/user/:id', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var user, error_7;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, prisma.user["delete"]({
                            where: {
                                id: Number(req.params.id)
                            }
                        })];
                case 1:
                    user = _a.sent();
                    if (!user) {
                        return [2 /*return*/, res.status(404).json({ message: 'User not found' })];
                    }
                    res.status(204).send();
                    return [3 /*break*/, 3];
                case 2:
                    error_7 = _a.sent();
                    res.status(500).json(error_7);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
}
exports.initializeUsers = initializeUsers;
