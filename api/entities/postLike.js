"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.initializePostLike = void 0;
function initializePostLike(app, prisma) {
    var _this = this;
    app.post('/post_like/', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var _a, userId, postId, postLike, error_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 2, , 3]);
                    _a = req.body, userId = _a.userId, postId = _a.postId;
                    return [4 /*yield*/, prisma.postLike.create({
                            data: {
                                User: { connect: { id: userId } },
                                Post: { connect: { id: postId } }
                            }
                        })];
                case 1:
                    postLike = _b.sent();
                    res.json(postLike);
                    return [3 /*break*/, 3];
                case 2:
                    error_1 = _b.sent();
                    res.status(500).json(error_1);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    // Get all PostLikes
    app.get('/post_likes/', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var postLike, error_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, prisma.postLike.findMany()];
                case 1:
                    postLike = _a.sent();
                    res.json(postLike);
                    return [3 /*break*/, 3];
                case 2:
                    error_2 = _a.sent();
                    res.status(500).json(error_2);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    // Get a specific PostLike by id
    app.get('/post_like/:id', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var postLike, error_3;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, prisma.postLike.findUnique({
                            where: {
                                id: Number(req.params.id)
                            }
                        })];
                case 1:
                    postLike = _a.sent();
                    if (!postLike) {
                        return [2 /*return*/, res.status(404).json({ error: 'PostLike not found' })];
                    }
                    res.json(postLike);
                    return [3 /*break*/, 3];
                case 2:
                    error_3 = _a.sent();
                    res.status(500).json(error_3);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    // Update a specific PostLike by id
    app.put('/post_like/:id', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var _a, userId, postId, postLike, error_4;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 2, , 3]);
                    _a = req.body, userId = _a.userId, postId = _a.postId;
                    return [4 /*yield*/, prisma.postLike.update({
                            where: {
                                id: Number(req.params.id)
                            },
                            data: {
                                User: { connect: { id: userId } },
                                Post: { connect: { id: postId } }
                            }
                        })];
                case 1:
                    postLike = _b.sent();
                    if (!postLike) {
                        return [2 /*return*/, res.status(404).send('PostLike not found')];
                    }
                    res.json(postLike);
                    return [3 /*break*/, 3];
                case 2:
                    error_4 = _b.sent();
                    res.status(500).json(error_4);
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); });
    // Delete a specific post by id
    app["delete"]('/post_like/:id', function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        var postLikeExist, error_5;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 3, , 4]);
                    return [4 /*yield*/, prisma.postLike.findUnique({
                            where: { id: Number(req.params.id) }
                        })];
                case 1:
                    postLikeExist = _a.sent();
                    if (!postLikeExist) {
                        return [2 /*return*/, res.status(404).send('PostLike not found')];
                    }
                    return [4 /*yield*/, prisma.postLike["delete"]({
                            where: { id: Number(req.params.id) }
                        })];
                case 2:
                    _a.sent();
                    res.status(200).send("PostLike with id ".concat(req.params.id, " has been deleted"));
                    return [3 /*break*/, 4];
                case 3:
                    error_5 = _a.sent();
                    res.status(500).json(error_5);
                    return [3 /*break*/, 4];
                case 4: return [2 /*return*/];
            }
        });
    }); });
}
exports.initializePostLike = initializePostLike;
