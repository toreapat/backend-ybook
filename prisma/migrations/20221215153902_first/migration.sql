/*
  Warnings:

  - You are about to drop the column `postId` on the `PostAttachment` table. All the data in the column will be lost.
  - You are about to drop the column `config` on the `User` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "PostAttachment" DROP CONSTRAINT "PostAttachment_postId_fkey";

-- AlterTable
ALTER TABLE "PostAttachment" DROP COLUMN "postId";

-- AlterTable
ALTER TABLE "User" DROP COLUMN "config";
